
// chat toggle (maximize/minimze)
$(".chat-toggle").on("click", function(){
	$(this).parents(".chat-box").toggleClass("is-minimized");
});


// make participant list are dragable
//  $('.content-area tbody').sortable();

//  slick slider
$('.js-slides2').slick({
	slidesToShow: 1,
	infinite: false,
	asNavFor: '.js-slides1'
});

$('.js-slides1').slick({
	slidesToShow: 6,
	infinite: false,
	asNavFor: '.js-slides2',
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 5
			}
		}
	] 
});
$('.slides-single').slick({
	slidesToShow: 6,
	infinite: false,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 5
			}
		}
	] 
});
var totSlide = $('.js-slides2 .slick-list .slick-track > div').length;
$('.total-slide').text(totSlide);

$('.js-slides2').on('afterChange', function(event, slick, currentSlide){
  var slide_length = slick.$slides.length;
  var slide_length_true = slide_length - 1;
  //console.log(currentSlide);
  	$('.js-prev').removeClass('disabled');
  	if(currentSlide == 0) {
  		$('.js-prev').addClass('disabled');
  	}
  	if(currentSlide == slide_length_true) {
  		$('.js-next').addClass('disabled');
  	} else {
  		$('.js-next').removeClass('disabled');
  	}
  	$('.current-slide').text(currentSlide + 1);

  	var dataAttr = $('.js-slides2 .slick-list .slick-track .slick-current .slide-item').data('img')
  	$('.js-img').attr({
     src: 'dist/images/' + dataAttr + '.png'
  	});
});
$('.js-next').on('click', function(){
	$('.js-slides2 .slick-next').trigger('click');
});
$('.js-prev').on('click', function(){
	$('.js-slides2 .slick-prev').trigger('click');
});
// accordion
$('.accordion-header').on('click', function (){
	$(this).parent().toggleClass('is-active');
});
videoWidth();
$(window).resize(function(){
	if ($(window).width() <= 1280) {
		sliderWidth();
		videoWidth();
	}
});
function sliderWidth() {
	var sidebarWidth = 360;
	var windowWidth = $(window).width();
	var slideWidth = windowWidth - sidebarWidth;
	$('.fixet-bottom').css('max-width', slideWidth);
}
function videoWidth() {
	var loginWidth = $('.js-login').width();
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var vidWidth = windowWidth - loginWidth;
	$('#myVideo').css('width', vidWidth);
	$('#myVideo').css('height', windowHeight);
}

$('.js-side').click(function(){
	$('.content-area, .sidebar-area').toggleClass('close-side');
});

